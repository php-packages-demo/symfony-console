# symfony-console

[symfony/console](https://packagist.org/packages/symfony/console)
![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/console)
Symfony Console Component.

![Debian popularity](https://qa.debian.org/cgi-bin/popcon-png?packages=php-symfony-filesystem+php-symfony-console+php-symfony-process+php-symfony-finder+php-symfony-expression-language+php-symfony-cache+php-symfony-config+php-symfony-dependency-injection+php-symfony-event-dispatcher&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%25Y)

# Official documentation
* [*The Console Component*](https://symfony.com/doc/current/components/console.html)
  * [*Using the Logger*
    ](https://symfony.com/doc/current/components/console/logger.html)
* Helpers inspired from Ruby on Rails
  * [*Open-Source cross-pollination*
    ](https://symfony.com/blog/open-source-cross-pollination)
    2008-09 Fabien Potencier
  * Which helpers
    * [*The Console Helpers*
      ](https://symfony.com/doc/current/components/console/helpers/index.html)
      * Formatter Helper
      * Process Helper
      * Progress Bar
      * Question Helper (replaces Dialog Helper)
      * Table
      * Debug Formatter Helper

## New features
* [*New in Symfony 5.3: Lazy Command Description*
  ](https://symfony.com/blog/new-in-symfony-5-3-lazy-command-description)
  2021-06 Javier Eguiluz
* [*New in Symfony 4.4: Horizontal Tables and Definition Lists in Console Commands*
  ](https://symfony.com/blog/new-in-symfony-4-4-horizontal-tables-and-definition-lists-in-console-commands)
  2019-10  Javier Eguiluz

# Unofficial documentation
* [*Easy Way to Create a Symfony Console Application*
  ](https://tanvir-ahmad.medium.com/easy-way-to-create-a-symfony-console-application-d173852c01cb)
  2021-12 Tanvir Ahmad
